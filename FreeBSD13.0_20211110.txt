--------------------------------------------------------
作業用PC:                       Dell G3 15 (3500)
作業用PCのOS:                   Linux Mint 20.2
インストールするPC:		Thinkpad X260
--------------------------------------------------------

[インストール媒体の作成]
$ cd ~/Downloads/
$ curl -O https://download.freebsd.org/ftp/releases/amd64/amd64/ISO-IMAGES/13.0/FreeBSD-13.0-RELEASE-amd64-memstick.img.xz

解凍
$ xz -dv FreeBSD-13.0-RELEASE-amd64-memstick.img.xz

USBメモリを挿入
$ sudo dd if=FreeBSD-13.0-RELEASE-amd64-memstick.img of=/dev/sdb bs=8M
$ sync


[インストール]
Enter -> F1
BIOS画面を表示させる

Date/Time
	UTC に合っていることを確認する

Security -> Secure Boot
	Secure Boot
		Enabled から Disabled に変更

Startup -> UEFI/Legacy Boot
	UEFI Only になっていることを確認

Startup -> Boot
	USBドライブを一番上に変更

Restart -> Exit Saving Changes
	Yes

1 を押す。
	1. Boot Multi User [Enter]

Welcome
<Install> を選択する。

Keymap Selection
	Continue with default keymap

Set Hostname
	x260

Distribution Select
[ ] base-dbg
[ ] kernel-dbg
[ ] lib32-dbg
[*] lib32
[*] ports
[ ] src
[ ] tests

How would you like to partition your disk?
	Auto (ZFS)

Swap Size:
	2g -> 16g

Select Virtual Device type:
	stripe	Stripe - No Redundancy

ZFS Configuration
[*] ada0

ZFS Configuration
	Yes

インストールが開始される

root password:	*********

Network Configuration
	em0

IPv4の設定
Would you like to configure IPv4 for this interface?
	<Yes>

Would you like to use
DHCP to configure this
interface?
	<No>

Network Configuration
IP Address:	192.168.1.XX
Subnet Mask:	255.255.255.0
Default Router:	192.168.1.1

IPv6の設定
Would you like to configure IPv6 for this interface?
	<No>

DNSの設定
Resolver Configuration
Search:		<空欄>
IPv4 DNS #1:	192.168.1.1
IPv4 DNS #2:	<空欄>

Is this machine's CMOS clock to set to UTC?
	<Yes>

Time Zone
	Asia -> Japan
	Skip
	Skip

System Configuration
Choose the services you would like to be started at boot:
[ ] local_unbound
[*] sshd
[ ] moused
[ ] ntpdate
[ ] ntpd
[*] powerd
[ ] dumpdev

System Hardening
[ ] Hide processes running as other users
[ ] Hide processes running as other groups
[ ] Hide processes running in jails
[ ] Disable reading kernel message buffer for unprivileged users
[ ] Disable process debugging facilities for unprivileged users
[ ] Randomize the PID of newly created processes
[*] Clean the /tmp filesystem on system startup
[ ] Disable opening Syslogd network socket (disable remote logging)
[*] Disable Sendmail service
[ ] Enable console password prompt
[ ] Disallow DTrace destructive-mode

Add User Accounts
Would you like to add users to the installed system now?
	<Yes>

Username:						takashi
Full name:						[Enter]
Uid:							1000
Login group [takashi]:					[Enter]
Invite takashi into other groups?:			wheel
Login class [default]:					[Enter]
Shell (sh csh tcsh nologin) [sh]:			sh
Home directory [/home/takashi]:				[Enter]
Home directory permissions (Leave empty for default):	[Enter]
Use password-based authentication? [yes]:		[Enter]
Use an empty password? [no]:				[Enter]
Use a random password? [no]:				[Enter]
Enter password:						*********
Lock out the account after creation? [no]:		[Enter]

Add another user?
	no

Exit を選択

Manual Configuration
The installation is now finished.
Before exiting the installer, would
you like to open a shell in the new
system to make any final manual
modifications?
	<No>

Complete
	<Reboot>

USBメモリを取り出す


[First Boot]
Enter->F1

Startup -> Boot
	FreeBSD が一番上にあることを確認する

1. Boot Multi User [Enter]
を選択する。


[時刻調整]
$ su -
# ntpdate ntp.nict.jp

確認
# date


[ターミナルでビープ音が鳴らないようにする]	未実施
X260 の場合はビープ音は鳴らないが、設定しておく。

# vi /etc/sysctl.conf
--------------------------------
kern.vt.enable_bell=0
--------------------------------

再起動する
# reboot
リブートされないので、電源オフ

kern.vt.enable_bell の設定が変更されていることを確認
# sysctl -a | grep kern.vt.enable
--------------------------------
kern.vt.enable_bell: 0
kern.vt.enable_altgr: 1
--------------------------------


[起動の待ち時間を変更]
# vi /boot/loader.conf
------------------------------
autoboot_delay="1"
------------------------------

なおデフォルト値は下記ファイルから確認できる
/boot/defaults/loader.conf


[ブートローダでのロゴの変更]
# vi /boot/loader.conf
------------------------------
loader_logo="fbsdbw"
------------------------------

OSを再起動する
# /sbin/reboot


[pkg]
# pkg update

The package management tool is not yet installed on your system.
Do you want to fetch and install it now?
-> y


[インストールされているパッケージのバージョンが最新か確認]
# pkg version -vR

[インストールしたパッケージを確認]
# pkg info

ファイル一覧
# pkg info -l <パッケージ名>


[パッケージの更新]
# pkg upgrade


[sudo]
バイナリでインストール
# pkg install sudo

# visudo
---------------------------
%wheel ALL=(ALL) NOPASSWD: ALL
---------------------------


[SSH認証鍵の追加]
known_hosts から IP:192.168.1.XX を削除する。
(別端末)$ ssh-keygen -f "/home/takashi/.ssh/known_hosts" -R "192.168.1.XX"

別端末のSSH認証鍵を IP:192.168.1.XX に配布する。
(別端末)$ ssh-copy-id -i ~/.ssh/id_ed25519.pub takashi@192.168.1.XX


[ログインメッセージを非表示にする]
$ touch ~/.hushlogin


[vim-console]
$ sudo pkg install vim

設定
$ vi ~/.vimrc
---------------------------
colorscheme elflord
set nowrapscan
set hlsearch
set foldmethod=marker
autocmd BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal g`\"" | endif
---------------------------


[bash]
$ sudo pkg install bash

デフォルトのシェルを変更する。
$ chsh
---------------------------------
Shell: /usr/local/bin/bash
---------------------------------


[coreutils]
$ sudo pkg install coreutils

GNU の ls などを使用できるようになる。

(使い方)
$ gls -F --color=auto


[bash の設定]
$ vi ~/.bash_profile
---------------------------------------
#!/usr/local/bin/bash

if [ -f $HOME/.bashrc ]; then
        source $HOME/.bashrc
fi
---------------------------------------

$ vi ~/.bashrc
---------------------------------------
#!/usr/local/bin/bash

# alias
alias ls='gls -F --color=auto'
alias vi='vim'
alias view='vim -R'
---------------------------------------


[X]
https://docs.freebsd.org/en/books/handbook/x11/

$ sudo pkg install xorg

video グループに ユーザ takashi を追加する。
$ sudo pw groupmod video -m takashi


[GPUの設定]
https://freebsddesktop.github.io/2019/03/11/xorg-quickstart.html

$ sudo pkg install drm-kmod

$ sudo sysrc kld_list+="/boot/modules/i915kms.ko"
-> /etc/rc.conf に追加される。

$ sudo /sbin/reboot

確認作業
$ dmesg | grep i915kms
drmn0: successfully loaded firmware image 'i915/skl_dmc_ver1_27.bin'
[drm] Finished loading DMC firmware i915/skl_dmc_ver1_27.bin (v1.27)
[drm] Initialized i915 1.6.0 20190822 for drmn0 on minor 0
drmn0: fb0: i915drmfb frame buffer device


[vlgothicフォント]
$ sudo pkg install ja-font-vlgothic


[notoフォント]
$ sudo pkg install noto-jp noto-emoji


[MATE]
https://docs.freebsd.org/en/books/handbook/x11/#x11-wm
https://www.linuxhelp.com/How-To-Install-Mate-Desktop-In-Freebsd

$ sudo pkg install slim
$ sudo pkg install mate

$ sudoedit /etc/fstab
-----------------------------------------------------
proc                    /proc   procfs  rw              0       0
-----------------------------------------------------

$ sudoedit /etc/rc.conf
-----------------------------------------------------
dbus_enable="YES"
hald_enable="YES"
slim_enable="YES"
-----------------------------------------------------

ログイン対象のデスクトップ環境を指定する。
$ vi ~/.xinitrc
-----------------------------------------------------
#!/bin/sh
exec mate-session
-----------------------------------------------------

$ sudo /sbin/reboot

-> SLIM のログイン画面が表示される。


[firefox]
https://docs.freebsd.org/en/books/handbook/desktop/#desktop-browsers

$ sudo pkg install firefox


[Terminal の設定]
MATE Terminal を開く。

Edit -> Profile Preferences

General
        [ ] Use the system fiexed width font
        Font:   Monospace Regular 14

        [ ] Terminal bell

        [*] Use custom default terminal size
        Default size: 90 x 40

Colors
	[ ] Use colors from system theme
	Bulit-in schemes:	White on black

        Palette
                Built-in schemes:       Tango


[fcitx]
https://blog.freebsd-days.com/2020/06/setup-fcitx-mozc/
https://freebsd.sing.ne.jp/desktop/06/08/04.html

$ sudo pkg install ja-fcitx-skk zh-fcitx-configtool

(設定)
$ vi ~/.xinitrc
----------------------------------------
export GTK_IM_MODULE=fcitx
export GTK3_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
----------------------------------------

$ mkdir ~/.config/autostart
$ cp -p /usr/local/share/applications/fcitx.desktop ~/.config/autostart/

MATE Desktop からログアウトして、ログインし直す。

$ fcitx-configtool &

Input Method
        Japanese -> Skk を追加する。

設定ファイルを開き、ショートカットを設定する。
(バグなのか、GUI から 小文字の l を指定できないため)
$ vi ~/.config/fcitx/config
----------------------------------------
# Custom
SwitchKey=Disabled

# Activate input method
ActivateKey=ALT_J
# Inactivate Input Method
InactivateKey=l
----------------------------------------

fcitx のアイコンを右クリックし、Restart を選択する。


[capslockをctrlに変更]
http://www.lifewithunix.jp/notes/2018/10/14/freebsd-11_1-keymap-name-for-ctrl-capslock-swap-was-changed/

$ sudoedit /etc/rc.conf
--------------------------------------
keymap="us.ctrl"
--------------------------------------

$ sudo /sbin/reboot

-> 端末での CapsLockキーが Ctrlキーに変更される。
   Xでは変更されない。


[bashの編集モードをviに変更]
$ vi ~/.inputrc
-------------------------------
set editing-mode vi
-------------------------------


[dconf-editor]
$ sudo pkg install dconf-editor

(使い方)
$ dconf-editor &


[MATE - capslockをctrlに変更]
MATE Desktop で端末を起動し、下記コマンドを実行する。
$ gsettings set org.mate.peripherals-keyboard-xkb.kbd options "['caps\tcaps:ctrl_modifier']"


[MATE - キーリピートの変更]
MATE Desktop で端末を起動し、下記コマンドを実行する。
$ gsettings set org.mate.peripherals-keyboard delay 300

$ sudo /sbin/reboot


[MATE - 新しく開いたウィンドウを中央に配置しない]
https://wiki.archlinux.org/title/MATE

$ dconf-editor &

/org/mate/marco/general/
	center-new-windows	false


[MATE - ウィンドウをスナップしない]
https://wiki.archlinux.org/title/MATE

$ dconf-editor &

/org/mate/marco/general/
	allow-tiling	false


[MATE - スクリーンセーバーを無効化]
System -> Preferences -> Look and Feel -> Screensaver

[ ] Activate screensaver when computer is idle


[Chicago95]
Windows95風デスクトップテーマ

$ sudo pkg install chicago95


[MATE Desktop の設定]
System -> Preferences -> Look and Feel -> Appearance

Theme:  Chicago95

Customize をクリックする。
Pointer:        MATE

Background:	#008080

Fonts
        Application font:       Noto Sans CJK JP Regular 12
        Document font:          Noto Sans CJK JP Regular 12
        Desktop font:           Noto Sans CJK JP Regular 11
        Window title font:      Noto Sans CJK JP Bold 12
        Fixed width font:       VL Gothic Regular 12


[mixer]	インストール済み
現在の設定を表示
$ mixer

音量調整
$ mixer <dev> <vol>
例.
$ mixer vol 90:90


[mixertui]
alsamixer のクローン

$ sudo pkg install mixertui

$ sudoedit /boot/loader.conf
------------------------------------
sysctlinfo_load="YES"
sysctlbyname_improved_load="YES"
------------------------------------

$ sudo /sbin/reboot

(使い方)
$ mixertui

$ vi ~/.bashrc
----------------------------------
alias am='mixertui'
----------------------------------


